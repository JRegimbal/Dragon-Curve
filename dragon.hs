import qualified Data.Sequence as Seq
import qualified Data.Foldable as Foldable
import Diagrams.Prelude
import Diagrams.Backend.SVG
import System.Environment

-- Generates the list of turns in a dragon curve of length x
dragon :: Integer -> [Bool]
dragon 1 = [True]
dragon n = Foldable.toList $ (Seq.fromList last) Seq.>< (Seq.fromList $ True:tailLast)
    where
        last = dragon $ n -1
        tailLast = Prelude.reverse . map not $ last

-- Creates a curve using the series of turns - True -> right, False -> left
curveMaker :: (Real t, Floating t) => [Bool] -> V2 t -> [V2 t]
curveMaker [] _ = []
curveMaker (x:xs) vLast =
    vCurrent:(curveMaker xs vCurrent)
    where
        vCurrent
            | x = vLast # rotateBy (1/4)
            | otherwise = vLast # rotateBy(-1/4)

diagram :: Integer-> Diagram B
diagram n =
    let 
        initialVector = (10 *^ unit_X)
        offsets = curveMaker (dragon n) initialVector
    in bg white (fromOffsets offsets)

main :: IO ()
main = do
    args <- getArgs
    let (iterations, filename) = parse args
    let size = mkSizeSpec $ V2 (Just 800) (Just 800)
    renderSVG filename size (diagram iterations)

parse :: [String] -> (Integer, String)
parse args
    | Prelude.length args == 0 = (5, "out.svg")
    | Prelude.length args == 1 = (iterations, "out.svg")
    | Prelude.length args >= 2 = (iterations, filename)
    where
        iterations = (read (head args) :: Integer)
        filename = (args !! 1) :: String
