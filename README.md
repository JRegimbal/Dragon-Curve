# Dragon-Curve

```
dragon                  (10 iterations to out.svg)
dragon n                (n iterations to out.svg)
dragon n fs             (n iterations to fs)
```

Requires [Diagrams](http://projects.haskell.org/diagrams/)

![Example Image](https://cdn.rawgit.com/JRegimbal/Dragon-Curve/master/out.svg)
